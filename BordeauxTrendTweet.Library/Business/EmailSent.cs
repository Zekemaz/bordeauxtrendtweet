using System;

namespace BordeauxTrendTweet.Library.Business
{
    public class EmailSent
    {
        public int Id { get; set; }
        public DateTime SentAt { get; set; }
        public string Category { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int IdUser { get; set; }
    }
}