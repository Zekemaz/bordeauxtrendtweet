using System.Collections.Generic;
using System.Linq;

namespace BordeauxTrendTweet.Library.Business.Mapper
{
    public class MapperEmailSent
    {
        public static Data.Entity.EmailSent Map(Business.EmailSent value)
        {
            return new Data.Entity.EmailSent
            {
                Id = value.Id,
                SentAt = value.SentAt,
                Category = value.Category,
                Subject = value.Subject,
                Body = value.Body,
                IdUser = value.IdUser,
            };
        }


        public static Business.EmailSent Map(Data.Entity.EmailSent value)
        {
            return new Business.EmailSent
            {
                Id = value.Id,
                SentAt = value.SentAt,
                Category = value.Category,
                Subject = value.Subject,
                Body = value.Body,
                IdUser = value.IdUser,
            };
        }

        public static List<Business.EmailSent> Map(List<Data.Entity.EmailSent> value)
        {
            return (from v in value select Map(v)).ToList();
        }
    }
}