using System.Collections.Generic;
using System.Linq;

namespace BordeauxTrendTweet.Library.Business.Mapper
{
    public class MapperSearchHistory
    {
        public static Data.Entity.SearchHistory Map(Business.SearchHistory value)
        {
            return new Data.Entity.SearchHistory
            {
                Id = value.Id,
                Criteria = value.Criteria,
                SearchType = value.SearchType,
                CreatedAt = value.CreatedAt,
                Text = value.Text,
                Url = value.Url,
                IsoLanguageCode = value.IsoLanguageCode,
                UserName = value.UserName,
                UserEntities = value.UserEntities,
                UserFollowersCount = value.UserFollowersCount,
                UserVerified = value.UserVerified,
                UserRetweetCount = value.UserRetweetCount,
                UserRetweeted = value.UserRetweeted,
                IdUser = value.IdUser,
            };
        }



        public static Business.SearchHistory Map(Data.Entity.SearchHistory value)
        {
            return new Business.SearchHistory()
            {
                Id = value.Id,
                Criteria = value.Criteria,
                SearchType = value.SearchType,
                CreatedAt = value.CreatedAt,
                Text = value.Text,
                Url = value.Url,
                IsoLanguageCode = value.IsoLanguageCode,
                UserName = value.UserName,
                UserEntities = value.UserEntities,
                UserFollowersCount = value.UserFollowersCount,
                UserVerified = value.UserVerified,
                UserRetweetCount = value.UserRetweetCount,
                UserRetweeted = value.UserRetweeted,
                IdUser = value.IdUser,

            };
        }
        public static List<Business.SearchHistory> Map(List<Data.Entity.SearchHistory> value)
        {
            return (from v in value select Map(v)).ToList();
        }
    }
}