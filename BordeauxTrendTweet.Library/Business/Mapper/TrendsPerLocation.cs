using System.Collections.Generic;
using System.Linq;

namespace BordeauxTrendTweet.Library.Business.Mapper
{
    public class MapperTrendsPerLocation
    {
        public static Data.Entity.TrendsPerLocation Map(Business.TrendsPerLocation value)
        {
            return new Data.Entity.TrendsPerLocation
            {
                Id = value.Id,
                Date = value.Date,
                LocationName = value.LocationName,
                Whoeid = value.Whoeid,
                Name = value.Name,
                Url = value.Url,
                PromotedContent = value.PromotedContent,
                Query = value.Query,
                TweetVolume = value.TweetVolume,
            };
        }



        public static Business.TrendsPerLocation Map(Data.Entity.TrendsPerLocation value)
        {
            return new Business.TrendsPerLocation
            {
                Id = value.Id,
                Date = value.Date,
                LocationName = value.LocationName,
                Whoeid = value.Whoeid,
                Name = value.Name,
                Url = value.Url,
                PromotedContent = value.PromotedContent,
                Query = value.Query,
                TweetVolume = value.TweetVolume,
            };
        }
        public static List<Business.TrendsPerLocation> Map(List<Data.Entity.TrendsPerLocation> value)
        {
            return (from v in value select Map(v)).ToList();
        }
    }
}