using System.Collections.Generic;
using System.Linq;

namespace BordeauxTrendTweet.Library.Business.Mapper
{
    public class MapperTrendsWorldwide
    {
        public static Data.Entity.TrendsWorldwide Map(Business.TrendsWorldwide value)
        {
            return new Data.Entity.TrendsWorldwide
            {
                Id = value.Id,
                Country = value.Country,
                CountryCode = value.CountryCode,
                Name = value.Name,
                ParentId = value.ParentId,
                PlaceTypeCode = value.PlaceTypeCode,
                PlaceTypeName = value.PlaceTypeName,
                Url = value.Url,
                Whoeid = value.Whoeid,
                Date = value.Date,

            };
        }



        public static Business.TrendsWorldwide Map(Data.Entity.TrendsWorldwide value)
        {
            return new Business.TrendsWorldwide
            {
                Id = value.Id,
                Country = value.Country,
                CountryCode = value.CountryCode,
                Name = value.Name,
                ParentId = value.ParentId,
                PlaceTypeCode = value.PlaceTypeCode,
                PlaceTypeName = value.PlaceTypeName,
                Url = value.Url,
                Whoeid = value.Whoeid,
                Date = value.Date,
            };
        }
        public static List<Business.TrendsWorldwide> Map(List<Data.Entity.TrendsWorldwide> value)
        {
            return (from v in value select Map(v)).ToList();
        }
    }
}