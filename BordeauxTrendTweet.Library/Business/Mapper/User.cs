using System.Collections.Generic;
using System.Linq;

namespace BordeauxTrendTweet.Library.Business.Mapper
{
    public class MapperUser
    {
        public static Data.Entity.User Map(Business.User value)
        {
            return new Data.Entity.User
            {
                // Id = value.Id,
                Uuid = value.Uuid,
                Firstname = value.Firstname,
                Lastname = value.Lastname,
                // Mobile = value.Mobile,
                // Email = value.Email,
                // Password = value.Password,
                TwitterUsername = value.TwitterUsername,
                // Role = value.Role,
                CreatedAt = value.CreatedAt,
            };
        }



        public static Business.User Map(Data.Entity.User value)
        {
            return new Business.User
            {
                Id = value.Id,
                Uuid = value.Uuid,
                Firstname = value.Firstname,
                Lastname = value.Lastname,
                // Mobile = value.Mobile,
                Email = value.Email,
                Password = value.PasswordHash,
                TwitterUsername = value.TwitterUsername,
                // Role = value.Role,
                CreatedAt = value.CreatedAt,
            };
        }
        public static List<Business.User> Map(List<Data.Entity.User> value)
        {
            return (from v in value select Map(v)).ToList();
        }
    }
}