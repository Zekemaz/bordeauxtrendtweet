﻿using System;
using System.Collections.Generic;
using BordeauxTrendTweet.Library.Data;
using BordeauxTrendTweet.Library.Service;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json.Linq;
using Org.BouncyCastle.Ocsp;
using RestSharp;
using RestSharp.Authenticators;

namespace BordeauxTrendTweet.Library.Business
{
    public class Request
    {
        private readonly MyDbContext _ctx;
        private ServiceSearchHistory _SearchHistoryService;

        public Request(MyDbContext context)
        {
            _ctx = context;
            _SearchHistoryService = new ServiceSearchHistory(_ctx);
        }
        public List<JToken> GetTweetBordeaux(string query, int id)
        {
            var client = new RestClient("https://api.twitter.com");
            client.Authenticator = OAuth1Authenticator.ForProtectedResource(
                "lYzxwdxinir1I5xjWDRnlRBeD", "RfSAbBnDciuyMXBwAuSc4hHa0A8aJMtHfz92W3QdlBHJ4cORYG", "", ""
            );
            var request = new RestRequest("/1.1/search/tweets.json", Method.GET);
            request.AddParameter("q", query, ParameterType.GetOrPost);
            request.AddParameter("count", 15);
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            var response = client.Execute(request);
            var json = JObject.Parse(response.Content);
            var script = new List<JToken>();
            foreach (var tweet in json["statuses"])
            {
                var url = "https://twitter.com/" + tweet["user"]["screen_name"] + "/status/" + tweet["id"];
                var client2 = new RestClient("https://publish.twitter.com");
                client2.Authenticator = OAuth1Authenticator.ForProtectedResource(
                    "lYzxwdxinir1I5xjWDRnlRBeD", "RfSAbBnDciuyMXBwAuSc4hHa0A8aJMtHfz92W3QdlBHJ4cORYG", "", ""
                );
                var request2 = new RestRequest("/oembed", Method.GET);
                request2.AddParameter("url", url);
                var response2 = client2.Execute(request2);
                var json2 = JObject.Parse(response2.Content);
                script.Add(json2["html"]);
            }
            
            
            foreach (var tweet in json["statuses"])
            {

                var searchHistory = new SearchHistory()
                {
                    Criteria = query,
                    SearchType = "Keyword",
                    TweetCreatedAt = DateTime.ParseExact((string)tweet["created_at"], 
                        "ddd MMM dd HH:mm:ss +ffff yyyy", new System.Globalization.CultureInfo("en-US")),
                    CreatedAt = DateTime.Now, 
                    Text = (string)tweet["text"] ,
                    Url = "https://twitter.com/" + tweet["user"]["screen_name"] + "/status/" + tweet["id"],
                    IsoLanguageCode = (string)tweet["metadata"]["iso_language_code"],
                    UserName = (string)tweet["user"]["name"],
                    UserFollowersCount = (int)tweet["user"]["followers_count"],
                    UserVerified = (bool)tweet["user"]["verified"],
                    UserRetweetCount = (int)tweet["retweet_count"],
                    UserRetweeted = (bool) tweet["retweeted"] == false ? 0 : 1,
                    IdUser = id,
                    UserEntities = "",
                };
                _SearchHistoryService.Post(searchHistory);
            }
            return script;
        }
        
        public static List<JToken> GetTrends(int woeid)
        {
            var client = new RestClient("https://api.twitter.com");
            client.Authenticator = OAuth1Authenticator.ForProtectedResource(
                "lYzxwdxinir1I5xjWDRnlRBeD", "RfSAbBnDciuyMXBwAuSc4hHa0A8aJMtHfz92W3QdlBHJ4cORYG", "", ""
            );
            var request = new RestRequest("/1.1/trends/place.json", Method.GET);
            request.AddParameter("id", woeid, ParameterType.GetOrPost);
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            var response = client.Execute(request);
            var json = JArray.Parse(response.Content);
            List<JToken> trendsList = new List<JToken>();
            foreach (var trends in json)
                trendsList.Add(JObject.Parse(trends.ToString()));
            return trendsList;
        }
    }
}