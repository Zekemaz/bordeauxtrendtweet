using System;

namespace BordeauxTrendTweet.Library.Business
{
    public class TrendsPerLocation
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string LocationName { get; set; }
        public int Whoeid { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string PromotedContent { get; set; }
        public string Query { get; set; }
        public int TweetVolume { get; set; }
    }
}