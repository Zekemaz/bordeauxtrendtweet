using System;

namespace BordeauxTrendTweet.Library.Business
{
    public class User
    {
        public int Id { get; set; }
        public string Uuid { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        // public string Mobile { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string TwitterUsername { get; set; }
        // public string Role { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}