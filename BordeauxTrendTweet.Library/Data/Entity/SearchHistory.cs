using System;

namespace BordeauxTrendTweet.Library.Data.Entity
{
    public class SearchHistory
    {
        public int Id { get; set; }
        public string Criteria { get; set; }
        public string SearchType { get; set; }
        public DateTime CreatedAt { get; set; }

        public DateTime TweetCreatedAt { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
        public string IsoLanguageCode { get; set; }
        public string UserName { get; set; }
        public string UserEntities { get; set; }
        public int UserFollowersCount { get; set; }
        public bool UserVerified { get; set; }
        public int UserRetweetCount { get; set; }
        public int UserRetweeted { get; set; }
        public int IdUser { get; set; }
    }
}