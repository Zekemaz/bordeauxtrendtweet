using System;

namespace BordeauxTrendTweet.Library.Data.Entity
{
    public class TrendsWorldwide
    {
        public int Id { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }
        public int PlaceTypeCode { get; set; }
        public string PlaceTypeName { get; set; }
        public string Url { get; set; }
        public int Whoeid { get; set; }
        public DateTime Date { get; set; }
    }
}