using System;
using Microsoft.AspNetCore.Identity;

namespace BordeauxTrendTweet.Library.Data.Entity
{
    public class User : IdentityUser<int>
    {
        public User()
        {
            Uuid = Guid.NewGuid().ToString();
            CreatedAt = DateTime.Now;
        }
        
        // public int Id { get; set; }
        public string Uuid { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        // public string Mobile { get; set; }
        // public string Email { get; set; }
        // public string Password { get; set; }
        public string TwitterUsername { get; set; }
        // public string Role { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}