using BordeauxTrendTweet.Library.Data.Entity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace BordeauxTrendTweet.Library.Data
{
    // public class MyDbContext :  DbContext
    public class MyDbContext : IdentityDbContext<User, Role, int>
    {
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public DbSet<User> User { get; set; }
        public DbSet<TrendsWorldwide> TrendsWorldwide { get; set; }
        public DbSet<TrendsPerLocation> TrendsPerLocation { get; set; }
        public DbSet<SearchHistory> SearchHistory { get; set; }
        public DbSet<EmailSent> EmailSent { get; set; }
    }
}