﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BordeauxTrendTweet.Library.Migrations
{
    public partial class SearchHistoryMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "TweetCreatedAt",
                table: "SearchHistory",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TweetCreatedAt",
                table: "SearchHistory");
        }
    }
}
