using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using MimeKit;

namespace BordeauxTrendTweet.Library.Service
{
    public class EmailSender : IEmailSender
    {
        private static readonly string FromAddress = "test@test.com"; // meilleur addresse pour éviter spam
        private static readonly string FromAddressTitle = "John Doe"; // Nom
        private static readonly string SmtpServer = "smtp.mailtrap.io"; 
        private static readonly int SmtpPort = 2525; 
        private static readonly string SmtpLogin = "bba5b3c475160e"; 
        private static readonly string SmtpPassword = "9e804bc2d43092";
        
        public Task SendEmailAsync(string email, string subject, string message)
        {
            return Execute(email, subject, message);
        }

        public async Task Execute(string email, string subject, string body)
        {
            var message = new MimeMessage();
            var builder = new BodyBuilder();
            
            message.From.Add(new MailboxAddress(FromAddressTitle, FromAddress));
            // foreach (var target in targetList)
            // {
            message.To.Add(new MailboxAddress(email.Split("@").First(), email));
            // }

            message.Subject = subject;
            builder.HtmlBody = body;
            message.Body = builder.ToMessageBody();

            var client = new SmtpClient();
            
            // client.AuthenticationMechanisms.Remove("XOAUTH2");
            await client.ConnectAsync(SmtpServer, SmtpPort);
            await client.AuthenticateAsync(SmtpLogin, SmtpPassword);

            await client.SendAsync(message);

            await client.DisconnectAsync(true);
        }
    }
}