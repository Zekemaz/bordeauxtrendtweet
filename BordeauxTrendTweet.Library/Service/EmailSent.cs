using System;
using System.Collections.Generic;
using System.Linq;
using BordeauxTrendTweet.Library.Business.Mapper;
using BordeauxTrendTweet.Library.Data;
using Microsoft.EntityFrameworkCore;

namespace BordeauxTrendTweet.Library.Service
{
    public class ServiceEmailSent
    {
        private readonly MyDbContext _ctx;
        public ServiceEmailSent(MyDbContext context)
        {
            _ctx = context;
        }
        public List<Business.EmailSent> Get()
        {
            //No tracking
            _ctx.EmailSent.AsNoTracking().ToList(); // Meilleur performance pour la lecture

            return MapperEmailSent.Map(_ctx.EmailSent.ToList());
        }
        
        public Business.EmailSent Get(int Id)
        {
            return MapperEmailSent.Map(_ctx.EmailSent.Where(u => u.Id == Id).FirstOrDefault());
        }
        
        
        public void Post(Business.EmailSent model)
        {
            var entity = MapperEmailSent.Map(model);
            _ctx.EmailSent.Add(entity);
            _ctx.SaveChanges();
        }

        public void Update(Business.EmailSent model)
        {
            var oldEmailSent = _ctx.EmailSent.Where(user => user.Id == model.Id).FirstOrDefault();
            oldEmailSent.Category = model.Category;
            oldEmailSent.Subject = model.Subject;
            oldEmailSent.Body = model.Body;
            oldEmailSent.IdUser = model.IdUser;
            oldEmailSent.SentAt = model.SentAt;
            _ctx.SaveChanges();
        }
        public void Delete(int id)
        {
            var entity = _ctx.EmailSent.Where(u => u.Id == id).FirstOrDefault();
            if (entity == null)
            {
                throw new Exception("L'entité n'a pas pu être trouvée");
            }
            _ctx.EmailSent.Remove(entity);
            _ctx.SaveChanges();
        }
    }
}