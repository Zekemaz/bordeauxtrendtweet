using System;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;

namespace BordeauxTrendTweet.Library.Service
{
    public static class MailerService
    {
        private static readonly string FromAddress = "test@test.com"; // meilleur addresse pour éviter spam
        private static readonly string FromAddressTitle = "John Doe"; // Nom
        private static readonly string SmtpServer = "smtp.mailtrap.io"; 
        private static readonly int SmtpPort = 2525; 
        private static readonly string SmtpLogin = "b239e8fe3acd89"; 
        private static readonly string SmtpPassword = "8d45f8fb53cd49";


        public static async Task<bool> SendAsync(string[] targetList, string subject, string body)
        {
            var message = new MimeMessage();
            var builder = new BodyBuilder();
            
            message.From.Add(new MailboxAddress(FromAddressTitle, FromAddress));
            foreach (var target in targetList)
            {
                message.To.Add(new MailboxAddress(target.Split("@").First(), target));
            }

            message.Subject = subject;
            builder.HtmlBody = body;
            message.Body = builder.ToMessageBody();

            var client = new SmtpClient();
            
            // client.AuthenticationMechanisms.Remove("XOAUTH2");
            await client.ConnectAsync(SmtpServer, SmtpPort);
            await client.AuthenticateAsync(SmtpLogin, SmtpPassword);

            await client.SendAsync(message);

            await client.DisconnectAsync(true);

            return true;
        }
        
    }
}