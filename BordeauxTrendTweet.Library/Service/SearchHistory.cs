using System;
using System.Collections.Generic;
using System.Linq;
using BordeauxTrendTweet.Library.Business.Mapper;
using BordeauxTrendTweet.Library.Data;
using Microsoft.EntityFrameworkCore;

namespace BordeauxTrendTweet.Library.Service
{
    public class ServiceSearchHistory
    {
        private readonly MyDbContext _ctx;
        public ServiceSearchHistory(MyDbContext context)
        {
            _ctx = context;
        }
        public List<Business.SearchHistory> Get()
        {
            //No tracking
            _ctx.SearchHistory.AsNoTracking().ToList();

            return MapperSearchHistory.Map(_ctx.SearchHistory.OrderByDescending(u=> u.CreatedAt).ToList());
        }
        public List<Business.SearchHistory> GetByUserId(int Id)
        {
            _ctx.SearchHistory.AsNoTracking().ToList();

            return MapperSearchHistory.Map(_ctx.SearchHistory.Where(u => u.IdUser == Id).OrderByDescending(u=> u.CreatedAt).ToList());
        }
        
        public Business.SearchHistory Get(int Id)
        {
            return MapperSearchHistory.Map(_ctx.SearchHistory.Where(u => u.Id == Id).FirstOrDefault());
        }
        
        
        public void Post(Business.SearchHistory model)
        {
            var entity = MapperSearchHistory.Map(model);
            _ctx.SearchHistory.Add(entity);
            _ctx.SaveChanges();
        }

        public void Update(Business.SearchHistory model)
        {
            var SearchHistory = _ctx.SearchHistory.Where(user => user.Id == model.Id).FirstOrDefault();
            SearchHistory.Criteria = model.Criteria;
            SearchHistory.SearchType = model.SearchType;
            SearchHistory.Text = model.Text;
            SearchHistory.Url = model.Url;
            SearchHistory.IsoLanguageCode = model.IsoLanguageCode;
            SearchHistory.UserName = model.UserName;
            SearchHistory.UserEntities = model.UserEntities;
            SearchHistory.UserFollowersCount = model.UserFollowersCount;
            SearchHistory.UserVerified = model.UserVerified;
            SearchHistory.UserRetweetCount = model.UserRetweetCount;
            SearchHistory.UserRetweeted = model.UserRetweeted;
            _ctx.SaveChanges();
        }
        public void Delete(int id)
        {
            var entity = _ctx.SearchHistory.Where(u => u.Id == id).FirstOrDefault();
            if (entity == null)
            {
                throw new Exception("L'entité n'a pas pu être trouvée");
            }
            _ctx.SearchHistory.Remove(entity);
            _ctx.SaveChanges();
        }
    }
}