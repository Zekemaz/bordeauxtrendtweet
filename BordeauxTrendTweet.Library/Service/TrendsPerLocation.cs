using System;
using System.Collections.Generic;
using System.Linq;
using BordeauxTrendTweet.Library.Business.Mapper;
using BordeauxTrendTweet.Library.Data;
using Microsoft.EntityFrameworkCore;

namespace BordeauxTrendTweet.Library.Service
{
    public class ServiceTrendsPerLocation
    {
        private readonly MyDbContext _ctx;
        public ServiceTrendsPerLocation(MyDbContext context)
        {
            _ctx = context;
        }
        public List<Business.TrendsPerLocation> Get()
        {
            //No tracking
            _ctx.TrendsPerLocation.AsNoTracking().ToList(); // Meilleur performance pour la lecture

            return MapperTrendsPerLocation.Map(_ctx.TrendsPerLocation.ToList());
        }
        
        public Business.TrendsPerLocation Get(int Id)
        {
            return MapperTrendsPerLocation.Map(_ctx.TrendsPerLocation.Where(u => u.Id == Id).FirstOrDefault());
        }
        
        
        public void Post(Business.TrendsPerLocation model)
        {
            var entity = MapperTrendsPerLocation.Map(model);
            _ctx.TrendsPerLocation.Add(entity);
            _ctx.SaveChanges();
        }

        public void Update(Business.TrendsPerLocation model)
        {
            var oldTrendsPerLocation = _ctx.TrendsPerLocation.Where(user => user.Id == model.Id).FirstOrDefault();

            oldTrendsPerLocation.Date = model.Date;
            oldTrendsPerLocation.LocationName = model.LocationName;
            oldTrendsPerLocation.Whoeid = model.Whoeid;
            oldTrendsPerLocation.Name = model.Name;
            oldTrendsPerLocation.Url = model.Url;
            oldTrendsPerLocation.PromotedContent = model.PromotedContent;
            oldTrendsPerLocation.Query = model.Query;
            oldTrendsPerLocation.TweetVolume = model.TweetVolume;
            _ctx.SaveChanges();
        }
        public void Delete(int id)
        {
            var entity = _ctx.TrendsPerLocation.Where(u => u.Id == id).FirstOrDefault();
            if (entity == null)
            {
                throw new Exception("L'entité n'a pas pu être trouvée");
            }
            _ctx.TrendsPerLocation.Remove(entity);
            _ctx.SaveChanges();
        }
    }
}