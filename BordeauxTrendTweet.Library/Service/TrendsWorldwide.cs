using System;
using System.Collections.Generic;
using System.Linq;
using BordeauxTrendTweet.Library.Business.Mapper;
using BordeauxTrendTweet.Library.Data;
using Microsoft.EntityFrameworkCore;

namespace BordeauxTrendTweet.Library.Service
{
    public class ServiceTrendsWorldwide
    {
        private readonly MyDbContext _ctx;
        public ServiceTrendsWorldwide(MyDbContext context)
        {
            _ctx = context;
        }
        public List<Business.TrendsWorldwide> Get()
        {
            //No tracking
            _ctx.TrendsWorldwide.AsNoTracking().ToList(); // Meilleur performance pour la lecture

            return MapperTrendsWorldwide.Map(_ctx.TrendsWorldwide.ToList());
        }
        
        public Business.TrendsWorldwide Get(int Id)
        {
            return MapperTrendsWorldwide.Map(_ctx.TrendsWorldwide.Where(u => u.Id == Id).FirstOrDefault());
        }
        
        
        public void Post(Business.TrendsWorldwide model)
        {
            var entity = MapperTrendsWorldwide.Map(model);
            _ctx.TrendsWorldwide.Add(entity);
            _ctx.SaveChanges();
        }

        public void Update(Business.TrendsWorldwide model)
        {
            var oldTrendsWorldwide = _ctx.TrendsWorldwide.Where(user => user.Id == model.Id).FirstOrDefault();

            oldTrendsWorldwide.Country = model.Country;
            oldTrendsWorldwide.CountryCode = model.CountryCode;
            oldTrendsWorldwide.Name = model.Name;
            oldTrendsWorldwide.ParentId = model.ParentId;
            oldTrendsWorldwide.PlaceTypeCode = model.PlaceTypeCode;
            oldTrendsWorldwide.PlaceTypeName = model.PlaceTypeName;
            oldTrendsWorldwide.Url = model.Url;
            oldTrendsWorldwide.Whoeid = model.Whoeid;
            oldTrendsWorldwide.Date = model.Date;
            _ctx.SaveChanges();
        }
        public void Delete(int id)
        {
            var entity = _ctx.TrendsWorldwide.Where(u => u.Id == id).FirstOrDefault();
            if (entity == null)
            {
                throw new Exception("L'entité n'a pas pu être trouvée");
            }
            _ctx.TrendsWorldwide.Remove(entity);
            _ctx.SaveChanges();
        }
    }
}