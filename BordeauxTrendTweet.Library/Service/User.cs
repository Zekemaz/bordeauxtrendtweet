using System;
using System.Collections.Generic;
using System.Linq;
using BordeauxTrendTweet.Library.Business.Mapper;
using BordeauxTrendTweet.Library.Data;
using Microsoft.EntityFrameworkCore;

namespace BordeauxTrendTweet.Library.Service
{
    public class ServiceUser
    {
        private readonly MyDbContext _ctx;
        public ServiceUser(MyDbContext context)
        {
            _ctx = context;
        }
        public List<Business.User> Get()
        {
            //No tracking
            _ctx.User.AsNoTracking().ToList(); // Meilleur performance pour la lecture

            return MapperUser.Map(_ctx.User.ToList());
        }
        
        public Business.User Get(int Id)
        {
            return MapperUser.Map(_ctx.User.Where(u => u.Id == Id).FirstOrDefault());
        }
        
        
        public void Post(Business.User model)
        {
            var entity = MapperUser.Map(model);
            _ctx.User.Add(entity);
            _ctx.SaveChanges();
        }

        public void Update(Business.User model)
        {
            // var oldUser = _ctx.User.Where(user => user.Id == model.Id).FirstOrDefault();
            //
            // oldUser.Uuid = model.Uuid;
            // oldUser.Firstname = model.Firstname;
            // oldUser.Lastname = model.Lastname;
            // // oldUser.Mobile = model.Mobile;
            // // oldUser.Email = model.Email;
            // // oldUser.Password = model.Password;
            // oldUser.TwitterUsername = model.TwitterUsername;
            // // oldUser.Role = model.Role;
            // oldUser.CreatedAt = model.CreatedAt;
            // _ctx.SaveChanges();
        }
        public void Delete(int id)
        {
            var entity = _ctx.User.Where(u => u.Id == id).FirstOrDefault();
            if (entity == null)
            {
                throw new Exception("L'entité n'a pas pu être trouvée");
            }
            _ctx.User.Remove(entity);
            _ctx.SaveChanges();
        }
    }
}