using Xunit;
using BordeauxTrendTweet.Library.Service;
using MimeKit;
using MailKit.Net.Smtp;
using System.Linq;

namespace BordeauxTrendTweet.UnitTests
{
    public class Mail
    {
        //Méthode de test d'envoi mail, [Fact] attribut déclare une méthode de test qui est exécutée par Test Runner
        [Fact]
        public void SendMail_IsTrue()
        {
            //Décalartion des variables
            string[] targetList;
            string subject, body;
            //Affectation des variables
            targetList = new string[] { "clementmarcel@outlook.fr", "dessere.gabriel@gmail.com" };
            subject = "Test Unitaire";
            body = "Le test unitaire fonctionne correctement ";

            //Initialisation de notre booléen sent, la fonction SendAsync de MailService retourne vrai si le mail s'est envoyé
            bool sent = MailerService.SendAsync(targetList, subject, body).GetAwaiter().GetResult();

            //Assert.True renvoi le message si la condition est fausse
            Assert.True(sent, "Mail pas envoyé");

        }
    }
}