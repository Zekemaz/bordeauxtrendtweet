using System;
using System.Security.Claims;
using System.Threading.Tasks;
using BordeauxTrendTweet.Library.Data;
using BordeauxTrendTweet.Library.Data.Entity;
using BordeauxTrendTweet.Library.Service;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BordeauxTrendTweet.Controllers
{
    public class AdminController : Controller
    {
        
        private readonly MyDbContext _ctx;
        private readonly UserManager<User> _userManager;
        private readonly ILogger<AdminController> _logger;
        private readonly ServiceUser _userService;
        private readonly ServiceSearchHistory _serviceSearchHistory;

        public AdminController(ILogger<AdminController> logger, MyDbContext context, UserManager<User> UserManager)
        {
            _logger = logger;
            _ctx = context;
            _userManager = UserManager;
            _userService = new ServiceUser(_ctx);
            _serviceSearchHistory = new ServiceSearchHistory(_ctx);
        }

      [HttpGet]
      public async Task<IActionResult> AdminHistory()
      {
          var user = await _userManager.GetUserAsync(User);
          ViewBag.user = await _userManager.GetRolesAsync(user);
          ViewBag.historyList = _serviceSearchHistory.Get();
          ViewBag.userService = _userService;
          //Pour printer son role, décommenter ces deux lignes :
          //string items = string.Join(Environment.NewLine, user);
          //Console.WriteLine(items);
          return View();
      }
    }
}