﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BordeauxTrendTweet.Models;
using BordeauxTrendTweet.Library.Data;
using BordeauxTrendTweet.Library.Data.Entity;
using BordeauxTrendTweet.Library.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace BordeauxTrendTweet.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly MyDbContext _ctx;
        private readonly UserManager<User> _userManager;
        private readonly ILogger<HomeController> _logger;
        private readonly ServiceUser _userService;
        private readonly ServiceSearchHistory _serviceSearchHistory;



        public HomeController(ILogger<HomeController> logger, MyDbContext context, UserManager<User> UserManager)
        {
            _logger = logger;
            _ctx = context;
            _userManager = UserManager;
            _userService = new ServiceUser(_ctx);
            _serviceSearchHistory = new ServiceSearchHistory(_ctx);

        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            ViewBag.listTrendWorld = Library.Business.Request.GetTrends(1);
            ViewBag.listTrendFrance = Library.Business.Request.GetTrends(580778);
            ViewBag.userList = _userService.Get();
            return View();
        }
        

        [HttpGet]
        public async Task<IActionResult> History()
        {
      
            var user = await _userManager.GetUserAsync(User);
            
            ViewBag.historyList = _serviceSearchHistory.GetByUserId(user.Id);
            
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}