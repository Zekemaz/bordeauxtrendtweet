﻿using System;
using System.Threading.Tasks;
using BordeauxTrendTweet.Library.Data;
using BordeauxTrendTweet.Library.Business;
using BordeauxTrendTweet.Library.Service;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Org.BouncyCastle.Ocsp;

namespace BordeauxTrendTweet.Controllers
{
    public class RequestsController : Controller
    {
        private UserManager<BordeauxTrendTweet.Library.Data.Entity.User> UserManager;
        private readonly ILogger<HomeController> _logger;
        private readonly MyDbContext _ctx;
        private readonly ServiceUser _userService;
        private readonly Request _request;

        public RequestsController(UserManager<BordeauxTrendTweet.Library.Data.Entity.User> userManager, ILogger<HomeController> logger, MyDbContext context)
        {
            UserManager = userManager;
            _logger = logger;
            _ctx = context;
            _userService = new ServiceUser(_ctx);
            _request = new Request(_ctx);
        }
        public IActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> Index(string query)
        {
            var user = await UserManager.FindByNameAsync(User.Identity.Name);
            ViewBag.requestList = _request.GetTweetBordeaux(query, user.Id);
            return View();
        }

        [HttpPost]
        public  IActionResult Trend(int woeid)
        {
            ViewBag.listTrendById = Library.Business.Request.GetTrends(woeid);
            return View();
        }

        [HttpGet]
        public IActionResult Trend()
        {
            return View();
        }
    }
}