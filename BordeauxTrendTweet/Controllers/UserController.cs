﻿using BordeauxTrendTweet.Library.Data;
using BordeauxTrendTweet.Library.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BordeauxTrendTweet.Controllers
{
    public class UserController : Controller
    {
        private readonly MyDbContext _ctx;
        private readonly ILogger<UserController> _logger;
        private readonly ServiceUser _userService;


        public UserController(ILogger<UserController> logger, MyDbContext context)
        {
            _logger = logger;
            _ctx = context;
            _userService = new ServiceUser(_ctx);
        }

        [HttpGet]
        public IActionResult Edit()
        {
            ViewBag.userList = _userService.Get();
            return View();
        }
    }
}