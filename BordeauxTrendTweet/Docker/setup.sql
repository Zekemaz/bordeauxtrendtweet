/*------------------------------------------------------------
*        Script SQLSERVER 
-- ------------------------------------------------------------*/

/*Création */
CREATE DATABASE trendtweet;
/*Execution de la commande*/
GO

/*Positionnement sur la base de  données tout juste créée*/
USE trendtweet;


/*------------------------------------------------------------
-- Table: [User]
------------------------------------------------------------*/
CREATE TABLE [User]
(
    Id              INT IDENTITY (1,1) NOT NULL,
    Uuid            VARCHAR(255)       NOT NULL,
    Firstname       VARCHAR(50)        NOT NULL,
    Lastname        VARCHAR(50)        NOT NULL,
    Mobile          VARCHAR(15)        NOT NULL,
    Email           VARCHAR(50)        NOT NULL,
    Password        VARCHAR(255)       NOT NULL,
    TwitterUsername VARCHAR(255)       NOT NULL,
    Role            VARCHAR(50)        NOT NULL,
    CreatedAt       DATETIME           NOT NULL,
    CONSTRAINT User_PK PRIMARY KEY (Id)
);


/*------------------------------------------------------------
-- Table: "SearchHistory"
------------------------------------------------------------*/
CREATE TABLE SearchHistory
(
    Id                 INT IDENTITY (1,1) NOT NULL,
    Criteria           VARCHAR(255)       NOT NULL,
    SearchType         VARCHAR(30)        NOT NULL,
    CreatedAt          DATETIME           NOT NULL,
    Text               VARCHAR(255)       NOT NULL,
    Url                VARCHAR(255)       NOT NULL,
    IsoLanguageCode    VARCHAR(3)         NOT NULL,
    UserName           VARCHAR(255)       NOT NULL,
    UserEntities       VARCHAR(255)       NOT NULL,
    UserFollowersCount INT                NOT NULL,
    UserVerified       bit                NOT NULL,
    UserRetweetCount   INT                NOT NULL,
    UserRetweeted      INT                NOT NULL,
    IdUser             INT
);


/*------------------------------------------------------------
-- Table: "EmailSent"
------------------------------------------------------------*/
CREATE TABLE EmailSent
(
    Id       INT IDENTITY (1,1) NOT NULL,
    SentAt   DATETIME           NOT NULL,
    Category VARCHAR(30)        NOT NULL,
    Subject  VARCHAR(100)       NOT NULL,
    Body     TEXT               NOT NULL,
    IdUser   INT,
    CONSTRAINT EmailSent_PK PRIMARY KEY (Id),
    CONSTRAINT EmailSent_User_FK FOREIGN KEY (IdUser) REFERENCES [User] (Id)
);


/*------------------------------------------------------------
-- Table: "TrendsPerLocation"
------------------------------------------------------------*/
CREATE TABLE TrendsPerLocation
(
    Id              INT IDENTITY (1,1) NOT NULL,
    Date            DATETIME           NOT NULL,
    LocationName    VARCHAR(100)       NOT NULL,
    Woeid           INT                NOT NULL,
    Name            VARCHAR(255)       NOT NULL,
    Url             VARCHAR(255)       NOT NULL,
    PromotedContent VARCHAR(255)       NOT NULL,
    Query           VARCHAR(255)       NOT NULL,
    TweetVolume     INT                NOT NULL,
    CONSTRAINT TrendsPerLocation_PK PRIMARY KEY (Id)
);


/*------------------------------------------------------------
-- Table: "TrendsWorldwide"
------------------------------------------------------------*/
CREATE TABLE TrendsWorldwide
(
    Id            INT IDENTITY (1,1) NOT NULL,
    Country       VARCHAR(50)        NOT NULL,
    CountryCode   VARCHAR(3)         NOT NULL,
    Name          VARCHAR(100)       NOT NULL,
    ParentId      INT                NOT NULL,
    PlaceTypeCode INT                NOT NULL,
    PlaceTypeName VARCHAR(255)       NOT NULL,
    Url           VARCHAR(255)       NOT NULL,
    Woeid         INT                NOT NULL,
    Date          DATETIME           NOT NULL,
    CONSTRAINT TrendsWorldwide_PK PRIMARY KEY (Id)
);


/*------------------------------------------------------------
-- Table: "UserTrendsWorldwide"
------------------------------------------------------------*/
CREATE TABLE UserTrendsWorldwide
(
    IdUser            INT NOT NULL,
    IdTrendsWorldwide INT NOT NULL,
    CONSTRAINT UserTrendsWorldwide_PK PRIMARY KEY (IdUser, IdTrendsWorldwide),
    CONSTRAINT UserTrendsWorldwide_User_FK FOREIGN KEY (IdUser) REFERENCES [User] (Id),
    CONSTRAINT UserTrendsWorldwide_TrendsWorldwide0_FK FOREIGN KEY (IdTrendsWorldwide) REFERENCES TrendsWorldwide (Id)
);


/*------------------------------------------------------------
-- Table: "UserTrendsPerLocation"
----------------------------------------- -------------------*/
CREATE TABLE UserTrendsPerLocation
(
    IdUser              INT NOT NULL,
    IdTrendsPerLocation INT NOT NULL,
    CONSTRAINT UserTrendsPerLocation_PK PRIMARY KEY (IdUser, IdTrendsPerLocation),
    CONSTRAINT UserTrendsPerLocation_User_FK FOREIGN KEY (IdUser) REFERENCES [User] (Id),
    CONSTRAINT UserTrendsPerLocation_TrendsPerLocation0_FK FOREIGN KEY (IdTrendsPerLocation) REFERENCES TrendsPerLocation (Id)
);

-- Table: [User]
INSERT INTO [User] (Uuid, Firstname, Lastname, Mobile, Email, Password, TwitterUsername, Role, CreatedAt)
VALUES ('1241341','John','Doe','0633446875','john.doe@gmail.com','Test1234','JohnDoe','ROLE_USER',GETDATE()),
       ('1611651','Jane','Dapie','0633446875','jane.dapie@gmail.com','Test1234','JaneDapie','ROLE_USER',GETDATE()),
       ('9864924','Janeau','Goeld','0633446875','janeau.goeld@gmail.com','Test1234','JaneauGoeld','ROLE_USER',GETDATE()),
       ('7984651','Johnny','Eara','0633446875','johnny.eara@gmail.com','Test1234','JohnnyEara','ROLE_USER',GETDATE()),
       ('9653211','Didier','Whao','0633446875','didier.whao@gmail.com','Test1234','DidierWhao','ROLE_USER',GETDATE()),
       ('2132164','Philippe','Gaoua','0633446875','philippe.gaoua@gmail.com','Test1234','PhilippeGaoua','ROLE_USER',GETDATE()),
       ('9864511','Denis','Lopez','0633446875','denis.lopez@gmail.com','Test1234','DenisLopez','ROLE_USER',GETDATE()),
       ('1715318','Antoine','Zery','0633446875','antoine.zery@gmail.com','Test1234','AntoineZery','ROLE_USER',GETDATE()),
       ('7874231','Ezekiel','Dido','0633446875','ezekiel.dido@gmail.com','Test1234','EzekielDido','ROLE_ADMIN',GETDATE());

-- Table: "SearchHistory"
INSERT INTO
  SearchHistory (Criteria,SearchType,CreatedAt,Text,Url,IsoLanguageCode,UserName,UserEntities,UserFollowersCount,UserVerified,UserRetweetCount,UserRetweeted,IdUser) 
  VALUES ("Poney", "???", GETDATE(), "Formidable requete sur les poneys", "", "FR", "JDOE", "DOE", 250, 1, 37, 28, 1),
  ("Poney", "???", GETDATE(), "Formidable requete sur les poneys", "", "FR", "JDOE", "DOE", 250, 1, 37, 28, 2),
  ("Poney", "???", GETDATE(), "Formidable requete sur les poneys", "", "FR", "JDOE", "DOE", 250, 1, 37, 28, 2),
  ("Poney", "???", GETDATE(), "Formidable requete sur les poneys", "", "FR", "JDOE", "DOE", 250, 1, 37, 28, 3);

-- Table: "EmailSent"
INSERT INTO EmailSent (SendAt, Category,Subject,Body,IdUser)
VALUES  (GETDATE(), "Categ1", "Envoi Mail", "Mail bien envoyé", 1),
        (GETDATE(), "Categ1", "Envoi Mail", "Mail bien envoyé", 1),
        (GETDATE(), "Categ1", "Envoi Mail", "Mail bien envoyé", 1);


-- Table: "TrendsPerLocation"
INSERT INTO TrendsPerLocation
(Date, LocationName, Woeid, Name, Url, PromotedContent, Query, TweetVolume) VALUES ();

-- Table: "TrendsWorldwide"

INSERT INTO TrendsWorldwide 
(Country, CountryCode, Name, ParentId, PlaceTypeCode, PlaceTypeName, Url, Woeid, Date) VALUES ();

-- Table: "UserTrendsWorldwide"

INSERT INTO UserTrendsWorldwide
( IdUser, IdTrendsWorldwide) VALUES ();

-- Table: "UserTrendsPerLocation"

INSERT INTO UserTrendsPerLocation 
(IdUser, IdTrendsPerLocation) VALUES ();